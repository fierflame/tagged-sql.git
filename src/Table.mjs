import defineProp from './defineProp.mjs';
import getId from './getId.mjs';
import Id from './Id.mjs';
import Field from './Field.mjs';
import Sql from './Sql.mjs';

/**
 *
 * @this {TaggedSql.Table?}
 * @param { string | TaggedSql.Id} table
 * @param {string | TaggedSql.Id | boolean} [alias]
 * @param {boolean} [global]
 * @returns {TaggedSql.Table}
 */
function TableConstructor(table, alias, global) {
	/** @type {TaggedSql.Table} */
	const that = this instanceof Table ? this : Object.create(Table.prototype);
	if (table instanceof Id){
		that.table = table.id;
	} else {
		that.table = table;
	}
	if (alias instanceof Id){
		that.alias = alias.id;
	} else if (typeof alias === 'string') {
		that.alias = alias;
	} else {
		that.alias = '';
	}
	if (typeof alias === 'boolean') {
		that.global = alias;
	} else if (typeof global === 'boolean') {
		that.global = global;
	} else {
		that.global = false;
	}
	return that;
}
/** @type {TaggedSql.TableConstructor} */
const Table = /** @type {*} */(TableConstructor);
Object.setPrototypeOf(Table.prototype, Sql.prototype);
Object.setPrototypeOf(Table, Sql);
defineProp(Table.prototype, '__values', () => []);
/**
 *
 * @param {TaggedSql.Table} param0
 * @param {'`' | '"'} [quote]
 * @returns {string}
 */
function toString({ table, alias }, quote) {
	if (!alias) { return getId(table, quote); }
	return `${ getId(table, quote) } AS ${ getId(alias, quote) }`;
}

defineProp(Table.prototype, 'build', function(_separator, quote) {
	return toString(this, quote);
});
defineProp(Table.prototype, '__template', function(quote) {
	return [toString(this, quote)];
});

defineProp(Table.prototype, 'transform', function(transformer) {
	const {table, alias, global} = this;
	/** @type {TaggedSql.Table} */
	const that = Object.create(Table.prototype);
	that.table = transformer(table, 'table', global);
	that.alias = alias && transformer(alias, 'alias');
	that.global = global;
	return that;
});

defineProp(Table.prototype, 'field', function(field) {
	return new Field(field, this);
});

defineProp(Table.prototype, 'as', function(alias) {
	/** @type {TaggedSql.Table} */
	const that = Object.create(Table.prototype);
	that.table = this.table;
	that.alias = alias || '';
	that.global = this.global;
	return that;
});

export default Table;
