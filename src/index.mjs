import defineProp from './defineProp.mjs';
import Field from './Field.mjs';
import Sql from './Sql.mjs';
import Expr from './Expr.mjs';
import FnExpr from './FnExpr.mjs';
import Fn from './Fn.mjs';
import Id from './Id.mjs';
import isLike from './isLike.mjs';
import Table from './Table.mjs';
import Group from './Group.mjs';


defineProp(Sql, 'Expr', Expr);
defineProp(Sql, 'FnExpr', FnExpr);
defineProp(Sql, 'Fn', Fn);
defineProp(Sql, 'Field', Field);
defineProp(Sql, 'Id', Id);
defineProp(Sql, 'Table', Table);
defineProp(Sql, 'Group', Group);
defineProp(Sql, 'isLike', isLike);
defineProp(Sql, 'version', '__VERSION__');


export default Sql;
