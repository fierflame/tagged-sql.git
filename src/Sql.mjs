import addTemplate from './addTemplate.mjs';
import defineGetter from './defineGetter.mjs';
import defineProp from './defineProp.mjs';
import link from './link.mjs';
import parse from './parse.mjs';


/**
 * @template {TaggedSql} T
 * @param {T} sql
 * @param {[string[], any[]]} result
 * @returns {T}
 */
function set(sql, result) {
	defineProp(sql, '___template', result[0]);
	defineProp(sql, '___values', result[1]);
	return sql;
}
/**
 *
 * @param {string[] | TemplateStringsArray} v
 * @returns {readonly string[]}
 */
function getTemplate(v) {
	return 'raw' in v && Array.isArray(v.raw) ? v.raw : v;
}
/**
 * @this {TaggedSql?}
 * @param  {...any} args
 * @returns {TaggedSql}
 */
function Constructor(...args) {
	/** @type {TaggedSql} */
	const sql = this instanceof Sql
		? this
		: Object.create(Sql.prototype);
		/** @type {[string[], any[]]} */
	const result =
		Array.isArray(args[0])
			? parse(getTemplate(args[0]), args.slice(1))
			: args.length ? link(args) : [[''], []];
	return set(sql, result);

}
/** @type {TaggedSql.Constructor} */
const Sql = /** @type {*} */(Constructor);

defineGetter(Sql.prototype, 'values', function (v) { return v.__values(); });
defineGetter(Sql.prototype, 'null', function(v) {
	const { ___values, ___template} = v;
	const template = !Array.isArray(___values) || !Array.isArray(___template) ? v.__template() : ___template;
	return template.length === 1 && /^\s*$/.test(template[0]);
});

defineProp(Sql.prototype, 'glue', function(...list) {
	if (!list.length) {
		/** @type {TaggedSql} */
		const sql = Object.create(Sql.prototype);
		return set(sql, [[''], []]);
	}
	list = list.flat();
	for (let i = list.length - 1; i > 0; i--) {
		list.splice(i, 0, this);
	}
	/** @type {TaggedSql} */
	const sql = Object.create(Sql.prototype);
	return set(sql, link(list));
});

defineProp(Sql.prototype, '__values', function () {
	const {___values, ___template} = this;
	if (!Array.isArray(___values) || !Array.isArray(___template)) { return []; }
	return ___values.map(v => v instanceof Sql ? v.__values() : [v]).flat();
});

defineProp(Sql.prototype, '__template', function (quote) {
	const { ___values, ___template} = this;
	if (!Array.isArray(___values) || !Array.isArray(___template)) { return ['']; }
	const others = ___template.slice(1);
	/** @type {string[]} */
	const template = [___template[0]];

	for (let i = 0; i < others.length; i++) {
		const v = ___values[i];
		const it = others[i];
		addTemplate(template, v, it, quote);
	}
	return template;
});

defineProp(Sql.prototype, 'build', function(separator, quote) {
	/** @type {string[]} */
	const template = this.__template(quote);

	if (typeof separator !== 'function') {
		return template.join(` ${ separator || '?' } `);
	}
	/** @type {string[]} */
	const strings = [template[0]];
	const {values} = this;
	for (let i = 1; i < template.length; i++) {
		const k = i - 1;
		strings.push(separator(values[k], k), template[i]);
	}
	return strings.join(' ');
});
defineProp(Sql.prototype, 'toString', function(quote) {
	return this.build('?', quote === '`' ? '`' : '"');
});
defineProp(Sql.prototype, 'transform', function(transformer) {
	const { ___values, ___template } = this;
	if (!Array.isArray(___values) || !Array.isArray(___template)) {
		return Sql();
	}
	/** @type {TaggedSql} */
	const sql = Object.create(Sql.prototype);
	defineProp(sql, '___template', [...___template]);
	defineProp(sql, '___values', ___values.map(v => {
		if (v instanceof Sql) {
			return v.transform(transformer);
		}
		return v;
	}));
	return sql;
});
defineProp(Sql.prototype, 'toTaggedSql', function() { return this; });
export default Sql;
