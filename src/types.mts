declare const Sql: Sql.Constructor;
interface Sql extends Sql.Like {
	readonly ___template?: readonly string[];
	readonly ___values?: readonly any[];
	readonly values: readonly any[];
	readonly null: boolean;
	__values(this: this): any[];
	__template(this: this, quote?: '`' | '"'): string[];
	transform(this: this, transformer: Sql.Transformer): this;
	toTaggedSql(this: this): this;
	build(this: this, separator?: Sql.Separator, quote?: '`' | '"'): string;
	toString(this: this, quote?: '`' | '"'): string;
	glue(this: this, ...list: (Sql.Item | Sql.Item[])[]): Sql;
}
declare namespace Sql {
	export interface Like {
		toTaggedSql(this: this): Sql;
	}
	export type Item = string | number | Like | undefined;
	export interface Constructor {
		(...list: Item[]): Sql;
		(sql: string[] | TemplateStringsArray, ...values: any[]): Sql;
		new(...list: Item[]): Sql;
		new(sql: string[] | TemplateStringsArray, ...values: any[]): Sql;
		prototype: Sql;
		Field: FieldConstructor;
		Table: TableConstructor;
		Id: IdConstructor;
		Expr: ExprConstructor;
		FnExpr: FnExprConstructor;
		Group: GroupConstructor;
		Fn: FnConstructor;
		isLike(v: any): v is Like;
		readonly version: '__VERSION__';
	}
	export interface FieldConstructor {
		(
			field: string | Sql.Id,
			table?: string | Table | Sql.Id,
			global?: boolean,
		): Field;
		new(
			field: string | Sql.Id,
			table?: string | Table | Sql.Id,
			global?: boolean,
		): Field;
		prototype: Field;
	}
	export interface Field extends Sql {
		field: string;
		table?: string;
		global?: boolean;
		alias?: boolean;
	}
	export interface TableConstructor {
		(table: string | Id, global?: boolean): Table;
		new(table: string | Id, global?: boolean): Table;
		(table: string | Id, alias: string | Id, global?: boolean): Table;
		new(table: string | Id, alias: string | Id, global?: boolean): Table;
		prototype: Table;
	}
	export interface Table extends Sql {
		table: string;
		alias: string;
		global?: boolean;
		field(this: this, field: string): Field;
		as(this: this, alias: string): Table;
	}
	export interface IdConstructor {
		(id: string, group?: string): Id;
		new(id: string, group?: string): Id;
		prototype: Id;
	}
	export interface Id extends Sql {
		id: string;
		group?: string;
	}
	export interface ExprConstructor {
		(operator: string, values: any[]): Expr;
		new(operator: string, values: any[]): Expr;
		prototype: Expr;
	}
	export interface Expr extends Sql {
		operator: string;
		operands: any[];
	}
	export interface FnExprConstructor {
		<T extends unknown[]>(name: string, args: T): FnExpr<T>;
		new<T extends unknown[]>(name: string, args: T): FnExpr<T>;
		prototype: FnExpr<unknown[]>;
	}
	export interface FnExpr<T extends unknown[]> extends Sql {
		name: string;
		args: T;
	}
	export interface FnConstructor {
		<T extends unknown[]>(name: string, length?: number): Fn<T>;
	}
	export interface Fn<T extends unknown[]> {
		(...p: T): FnExpr<T>;
		name: string;
		length: number;
	}
	export interface GroupConstructor {
		(values: any[] | any): Group;
		new(values: any[] | any): Group;
		prototype: Group;
	}
	export interface Group extends Sql {
		children: any[];
	}
	export interface Transformer {
		(id: string, group?: string, global?: boolean): string
	}
	export interface SeparatorFn {
		(value: any, index: number): string
	}
	export type Separator = string | SeparatorFn;
}
export default Sql;
