import Sql from './Sql.mjs';
import addTemplate from './addTemplate.mjs';
import defineProp from './defineProp.mjs';

/**
 * @this {TaggedSql.Group?}
 * @param {*} children
 * @returns {TaggedSql.Group}
 */
function GroupConstructor(children) {
	/** @type {TaggedSql.Group} */
	const that = this instanceof Group ? this : Object.create(Group.prototype);
	that.children = Array.isArray(children) ? children : [children];
	return that;
}

/** @type {TaggedSql.GroupConstructor} */
const Group = /** @type {*} */(GroupConstructor);

Object.setPrototypeOf(Group.prototype, Sql.prototype);
Object.setPrototypeOf(Group, Sql);

defineProp(Group.prototype, '__template', function(quote) {
	const values = this.children.filter(v => v !== undefined);
	if (!values.length) { return [('(NULL)')]; }
	/** @type {string[]} */
	const template = ['('];
	const end = values.pop();
	for (const other of values) {
		addTemplate(template, other, ',', quote);
	}
	addTemplate(template, end, ')', quote);
	return template;
});

defineProp(Group.prototype, '__values', function() {
	const { children } = this;
	return children.map(v => v instanceof Sql ? v.__values() : v === undefined ? [] : [v]).flat();
});

defineProp(Group.prototype, 'transform', function(transformer) {
	const {children} = this;
	return Group(children.map(v => v instanceof Sql ? v.transform(transformer) : v));
});
export default Group;
