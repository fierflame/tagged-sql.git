import Sql from './Sql.mjs';

/**
 *
 * @param {string[]} template
 * @param {string} str
 * @returns
 */
export function appendTemplate(template, str) {
	if (!str) { return; }
	const end = template.pop();
	template.push(end ? [end, str].join(' ') : str);
}


/**
 *
 * @param {string[]} template
 * @param {*} value
 * @param {string} next
 * @param {"`" | "\""} [quote]
 * @returns
 */
export default function addTemplate(template, value, next, quote) {
	if (!(value instanceof Sql)) {
		template.push(next);
		return;
	}
	const list = value.__template(quote);
	const end = template.pop();
	if (end) {
		const [start] = list;
		list[0] = start ? [end, start].join(' ') : end;
	}
	appendTemplate(list, next);
	for (const t of list) {
		template.push(t);
	}
}
