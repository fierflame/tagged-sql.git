/**
 * @template {{}} T
 * @template {keyof T} K
 * @param {T} target
 * @param {K} name
 * @param {T[K]} value
 * @returns {void}
 */
export default function defineProp(target, name, value) {
	Reflect.defineProperty(target, name, { value, configurable: true });
}
