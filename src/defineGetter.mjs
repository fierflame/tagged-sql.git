import defineProp from './defineProp.mjs';
/**
 * @template {{}} T
 * @template {keyof T} K
 * @param {T} target
 * @param {K} name
 * @param {(sql: T) => T[K]} get
 * @returns {void}
 */
export default function defineGetter(target, name, get) {
	Reflect.defineProperty(target, name, {
		/** @this {T} */
		get() {
			const value = get(this);
			defineProp(this, name, value);
			return value;
		},
		configurable: true,
	});
}
