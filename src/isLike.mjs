/**
 *
 * @param {*} v
 * @returns {v is TaggedSql.Like}
 */
export default function isLike(v) {
	if (!v) { return false; }
	if (typeof v !== 'object' && typeof v !== 'function') { return false; }
	return typeof v.toTaggedSql === 'function';
}
