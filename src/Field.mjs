import defineProp from './defineProp.mjs';
import Sql from './Sql.mjs';
import getId from './getId.mjs';
import Id from './Id.mjs';
import Table from './Table.mjs';

/**
 * @this {TaggedSql.Field?}
 * @param {string | TaggedSql.Id} field
 * @param {string | TaggedSql.Table | TaggedSql.Id} [table]
 * @param {boolean} [global]
 * @returns {TaggedSql.Field}
 */
function FieldConstructor(field, table, global) {
	/** @type {TaggedSql.Field} */
	const that = this instanceof Field
		? this
		: Object.create(Field.prototype);
	if (field instanceof Id){
		that.field = field.id;
	} else {
		that.field = field;
	}
	if (table instanceof Table){
		const { alias } = table;
		if (alias) {
			that.table = alias;
			that.alias = true;
		} else {
			that.table = table.table;
		}
		that.global = typeof global === 'boolean'
			? global
			: table.global;
	} else if (table instanceof Id){
		that.table = table.id;
		that.global = global;
		if (table.group === 'alias') {
			that.alias = true;
		}
	} else {
		that.table = table;
		that.global = global;
	}
	return that;

}

/** @type {TaggedSql.FieldConstructor} */
const Field = /** @type {*} */(FieldConstructor);
Object.setPrototypeOf(Field.prototype, Sql.prototype);
Object.setPrototypeOf(Field, Sql);

defineProp(Field.prototype, '__values', () => []);
/**
 *
 * @param {TaggedSql.Field} param0
 * @param {'`' | '"'} [quote]
 * @returns {string}
 */
function toString({table, field}, quote) {
	if (table) {
		return `${ getId(table, quote) }.${ getId(field, quote) }`;
	}
	return getId(field, quote);
}
defineProp(Field.prototype, 'build', function(_separator, quote) {
	return toString(this, quote);
});
defineProp(Field.prototype, '__template', function(quote) {
	return [toString(this, quote)];
});

defineProp(Field.prototype, 'transform', function(transformer) {
	const { field, table, alias, global } = this;
	/** @type {TaggedSql.Field} */
	const that = Object.create(Field.prototype);
	that.field = transformer(field, 'field');
	that.alias = alias;
	if (!table) { return that; }
	if (alias) {
		that.table = transformer(table, 'alias');
	} else {
		that.table = transformer(table, 'table', global);
	}
	that.global = global;
	return that;
});

export default Field;
