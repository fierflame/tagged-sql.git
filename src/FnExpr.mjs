import Sql from './Sql.mjs';
import addTemplate from './addTemplate.mjs';
import defineProp from './defineProp.mjs';

/**
 * @template {unknown[]} T
 * @this {TaggedSql.FnExpr<T>?}
 * @param {string} name
 * @param {T} args
 * @returns {TaggedSql.FnExpr<T>}
 */
function FnExprConstructor(name, args) {
	/** @type {TaggedSql.FnExpr<T>} */
	const that = this instanceof FnExpr
		? this
		: Object.create(FnExpr.prototype);
	that.name = name;
	that.args = args || [];
	return that;
}
/** @type {TaggedSql.FnExprConstructor} */
const FnExpr = /** @type {*} */(FnExprConstructor);
Object.setPrototypeOf(FnExpr.prototype, Sql.prototype);
Object.setPrototypeOf(FnExpr, Sql);

defineProp(FnExpr.prototype, '__template', function(quote) {
	const { args: [...args], name} = this;
	if (!args.length) { return [`${ name }()`]; }
	/** @type {string[]} */
	const template = [`${ name }(`];
	const last = args.pop();

	/**
	 *
	 * @param {any} v
	 * @param {string} end
	 */
	function add(v, end) {
		addTemplate(template, v, end, quote);
	}
	for (const v of args) {
		add(v, ',');
	}
	add(last, ')');
	return template;
});

defineProp(FnExpr.prototype, '__values', function() {
	const { args } = this;
	return args.map(v => v instanceof Sql ? v.__values() : [v]).flat();
});

defineProp(FnExpr.prototype, 'transform', function(transformer) {
	return FnExpr(
		transformer(this.name, 'fn'),
		this.args.map(v => v instanceof Sql ? v.transform(transformer) : v),
	);
});
export default FnExpr;
